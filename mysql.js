module.exports = (db) => ({
    getItems(resource) {
        return new Promise((res, rej) => {
            db.query("SELECT * FROM ??", [resource], (err, result) => {
                if (err) {
                    rej(err);
                }
                const shortInfo = [];
                result.map( r => {
                    if (r.category_id !== undefined && r.place_id !== undefined) {
                        shortInfo.push({
                            title: r.title,
                            id: r.id,
                            category_id: r.category_id,
                            place_id: r.place_id,
                        });
                    } else {
                        shortInfo.push({title: r.title, id: r.id,});
                    }
                });
                res(shortInfo);
            });
        });
    },
    getItem(resource, id) {
        return new Promise((res, rej) => {
            db.query("SELECT * FROM ?? WHERE id = ?", [resource, id], (err, result) => {
                if (err) {
                    rej(err);
                }
                res(result);
            });
        });
    },
    createItem(resource, data) {
        return new Promise((res, rej) => {
            db.query("INSERT INTO ?? SET ?", [resource, data], (err, result) => {
                if (err) {
                    rej(err);
                }
                data.id = result.insertId;
                res(data);
            });
        });
    },
    deleteItem(resource, id) {
        return new Promise((res, rej) => {
            db.query("DELETE FROM ?? WHERE id = ?", [resource, id], (err, result) => {
                if (err) {
                    console.log(err);
                    rej(err);
                }
                res(result);
            });
        });
    },
});