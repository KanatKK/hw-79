const router = require("express").Router();
const multer = require("multer");
const path = require("path");
const {nanoid} = require("nanoid");

const storage = multer.diskStorage({
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const createRouter = (db) => {
  //items//////////////////////////////////////////////////////////////////////////////////////////////
  router.get("/items", async (req, res) => {
    try {
      const items = await db.getItems("items");
      res.send(items);
    } catch (e) {
      res.status(500).send(e);
    }
  });

  router.get("/items/:id", async (req, res) => {
    const response = await db.getItem("items" ,req.params.id);
    res.send(response[0]);
  });

  router.post("/items", upload.single("image"), async (req, res) => {
    const item = req.body;
    if (req.file) {
      item.image = req.file.filename;
    }
    const newItem = await db.createItem("items", item);
    res.send(newItem);
  });

  router.delete("items/:id", async (req, res) => {
    await db.deleteItem("items", req.params.id);
    const response = await db.getItems("items");
    res.send(response);
  });

  //categories//////////////////////////////////////////////////////////////////////////////////////////////
  router.get("/categories", async (req, res) => {
    try {
      const categories = await db.getItems("categories");
      res.send(categories);
    } catch (e) {
      res.status(500).send(e);
    }
  });

  router.get("/categories/:id", async (req, res) => {
    const response = await db.getItem("categories" ,req.params.id);
    res.send(response[0]);
  });

  router.post("/categories", upload.single("image"), async (req, res) => {
    const category = req.body;
    const newCategory = await db.createItem("categories", category);
    res.send(newCategory);
  });

  router.delete("categories/:id", async (req, res) => {
    await db.deleteItem("categories", req.params.id);
    const response = await db.getItems("categories");
    res.send(response);
  });

  //places//////////////////////////////////////////////////////////////////////////////////////////////
  router.get("/places", async (req, res) => {
    try {
      const places = await db.getItems("places");
      res.send(places);
    } catch (e) {
      res.status(500).send(e);
    }
  });

  router.get("/places/:id", async (req, res) => {
    const response = await db.getItem("places" ,req.params.id);
    res.send(response[0]);
  });

  router.post("/places", upload.single("image"), async (req, res) => {
    const place = req.body;
    const newPlace = await db.createItem("places", place);
    res.send(newPlace);
  });

  router.delete("places/:id", async (req, res) => {
    await db.deleteItem("places", req.params.id);
    const response = await db.getItems("places");
    await res.send(response);
  });

  return router;
};

module.exports = createRouter;